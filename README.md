
# NestJS Example API

## Description

API basée sur le framework [Nest](https://github.com/nestjs/nest).

## Installation

```bash
$ npm install
```

## Lancement de l'application

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# test unitaires
$ npm run test

# test coverage
$ npm run test:cov
```

## License

  [MIT licensed](LICENSE)
