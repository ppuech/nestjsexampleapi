export interface ApiServiceInterface<T> {
  /**
   * Récupère l'ensemble des objets enregistrés
   * @returns {Array<T>} un tableau d'objet
   */
  selectAll(): Array<T>;

  /**
   * Récupère un objet enregistré à partir d'un id passé en paramètre.
   * @param {string} id l'id de l'objet à récupérer
   * @returns {T} l'objet enregistré dont l'id correspond au paramètre
   */
  select(id: string): T;

  /**
   * Enregistre un objet en base
   * @param {T} object l'objet à enregistrer
   */
  insert(object: T): T;

  /**
   * Met à jour un objet déjà enregistré
   * @param {T} object l'objet modifié à enregistrer
   */
  update(object: T): T;

  /**
   * Supprime un objet
   * @param {string} id l'id de l'objet à supprimer
   */
  delete(id: string): void;
}
