export interface DtoMapper<K, V> {
  /**
   * Permet de convertir un objet DTO en un model de données
   * @param {K} objectDTO l'objet à convertir
   * @returns {V} un model de données
   */
  swap(objectDTO: K): V;
}
