import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { ObjectModule } from './objects/objects.module';

@Module({
  imports: [ObjectModule],
  controllers: [],
  providers: [AppService],
})
export class AppModule {}
