import { Injectable } from '@nestjs/common';
import { ApiServiceInterface } from 'src/sahred/interface/api-service.interface';
import { Book } from './book.model';

@Injectable()
export class BookService implements ApiServiceInterface<Book> {
  private database: Book[] = [
    {
      id: '1',
      author: 'testeght',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '2',
      author: 'test',
      title: 'ght',
      dateCreation: new Date(),
    },
    {
      id: '3',
      author: 'tesrgehgr-tht',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '4',
      author: 'tesgrrggdkit',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '5',
      author: '_ikkiki',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '6',
      author: 'w<xsxs',
      title: 'blabla',
      dateCreation: new Date(),
    },
  ];

  selectAll(): Book[] {
    return this.database;
  }

  select(id: string): Book {
    return this.database.find(book => book.id == id);
  }

  insert(object: Book): Book {
    if (!object.dateCreation) {
      object.dateCreation = new Date();
    }
    this.database.push(object);
    return object;
  }

  update(object: Book): Book {
    return (this.database[
      this.database.findIndex(book => book.id === object.id)
    ] = object);
  }

  delete(id: string): void {
    this.database.splice(
      this.database.findIndex(book => book.id === id),
      1,
    );
  }
}
