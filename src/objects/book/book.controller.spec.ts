import { BookController } from './book.controller';
import { BookService } from './book.service';
import { Book } from './book.model';

describe('BookController', () => {
  let bookController: BookController;
  let bookService: BookService;
  const fullList: Book[] = [
    {
      id: '1',
      author: 'testeght',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '2',
      author: 'test',
      title: 'ght',
      dateCreation: new Date(),
    },
    {
      id: '3',
      author: 'tesrgehgr-tht',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '4',
      author: 'tesgrrggdkit',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '5',
      author: '_ikkiki',
      title: 'blabla',
      dateCreation: new Date(),
    },
    {
      id: '6',
      author: 'w<xsxs',
      title: 'blabla',
      dateCreation: new Date(),
    },
  ];

  beforeEach(() => {
    bookService = new BookService();
    bookController = new BookController(bookService);
  });

  describe('findAll', () => {
    it('should return an array of books', async () => {
      jest.spyOn(bookService, 'selectAll').mockImplementation(() => fullList);
      expect(await bookController.findAll()).toBe(fullList);
    });
  });

  describe('find', () => {
    it('should return a book', async () => {
      jest.spyOn(bookService, 'select').mockImplementation(() => fullList[3]);
      expect(await bookController.find('4')).toBe(fullList[3]);
    });
  });
});
