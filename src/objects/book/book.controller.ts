import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  HttpStatus,
} from '@nestjs/common';
import { BookService } from './book.service';
import { Book } from './book.model';

@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  /**
   * Endpoint de récupération des livres
   */
  @Get()
  findAll() {
    return this.bookService.selectAll();
  }

  @Get(':id')
  find(@Param('id') id: string) {
    return this.bookService.select(id);
  }

  @Post()
  create(@Body() book: Book) {
    return this.bookService.insert(book);
  }

  @Put()
  update(@Body() book: Book) {
    return this.bookService.update(book);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    this.bookService.delete(id);
    return HttpStatus.OK;
  }
}
