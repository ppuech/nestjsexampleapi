import { IsNotEmpty, IsString } from 'class-validator';

export class Book {
  id: string;

  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  author: string;

  dateCreation: Date;
}
